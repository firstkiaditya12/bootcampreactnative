// Tugas 1
console.log("Asynchronous")
console.log("=====Tugas 1=====")
let readBooks = require('./callback.js')

let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

let t = 10000
let i = 0;

function panggilcallback(){
    readBooks(t, books[i], function(sisawaktu){
        t - sisawaktu
        i++
        if(i < books.length)
        panggilcallback()
    })  
}

panggilcallback();