// Tugas 1 Looping While
    console.log("-----Tugas 1 Looping While-----");
    console.log("Looping Pertama");
    var angka = 2;
    while (angka <= 20) {
        console.log(angka + " - I love coding");
        angka+=2;
    }
    console.log("Looping Kedua");
    var deret = 20;
    while (deret >= 2) {
        console.log(deret + " - I will become a mobile developer");
        deret-=2;
    }

// Tugas 2 Looping For
    console.log("-----Tugas 2 Looping For-----");
    var santai = " - santai";
    var berkualitas = " - berkualitas";
    var coding = " - I love coding";
    for (let i = 1; i <= 20; i++) {
        //genap
        if (i % 2 != 1) {
            console.log(i + berkualitas)
        //bilangan kelipatan 3
        } else if(i % 3 == 0){
            console.log(i + coding)
        } else {
        //ganjil
            console.log(i + santai)
        }
    }

// Tugas 3 Membuat Persegi Panjang
    console.log("-----Tugas 3 Persegi Panjang-----");
    var m = 1;
    var n = 1;
    var panjang = 8;
    var lebar = 4;
    var pagar = " ";
    while (n <= lebar) {
        while (m <= panjang) {
            pagar += "#";
            m++;
        }
        console.log(pagar);
        pagar=' ';
        m=1;
        n++;
    }

// Tugas 4 Segitiga
    console.log("-----Tugas 4 Segitiga-----");
    var pager = "";
    for (let k = 0; k <= 7; k++) {
        for (let l = 0; l <= k; l++) {
            pagar += "#";
        }
        console.log(pagar);
        pagar = " ";
    }

// Tugas 5 Papan Catur
    console.log("-----Tugas 5 Papan Catur-----");
    var h = 1;
    var g = 1;
    var panjang_catur = 8;
    var lebar_catur = 8;
    var Papan = "";
    for (let g = 0; g <= lebar_catur ; g++) {
        if (g % 2 ==1) {
            for (let h = 0; h <= panjang_catur; h++) {
                if (h % 2 == 1) {
                    Papan += " "
                } else {
                    Papan += "#"
                }
            }
        } else {
            for (let h = 0; h <= panjang_catur; h++) {
                if (h % 2 == 1) {
                    Papan += "#"
                } else {
                    Papan += " "
                }
            }
        }
        
    }
