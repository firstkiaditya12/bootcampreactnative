// Tugas 1
console.log("=====Tugas 1=====");
function teriak() {
    return "Halo Sanbers!"
}
console.log(teriak())

// Tugas 2
console.log("=====Tugas 2=====");
function kalikan(num1, num2) {
    return num1 * num2
}

var num1 = 12
var num2 = 4

var hasilkali = kalikan(num1, num2)
console.log(hasilkali) //48

// Tugas 3
console.log("=====Tugas 3=====");
function introduce(name, age, address, hobby) {
    return "Nama saya "+name+",umur saya "+age+",alamat saya "+address+",dan saya punya hobby yaitu "+hobby
}
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)