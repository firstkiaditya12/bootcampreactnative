// Tugas 1
function arrayToObject(arr) {
    if (arr.length <= 0) {
        return console.log("")
    }
    for (var i = 0; i < arr.length; i++) {
        var newObject = {}
        var birthYear = arr[i][3];
        var now = new Date().getFullYear()
        var newAge;
        if (birthYear && now - birthYear > 0) {
            newAge = now - birthYear
        } else {
            newAge = "Invalid Birth Year"
        }
        newObject.firstname = arr[i][0]
        newObject.lastname = arr[i][1]
        newObject.gender = arr[i][2]
        newObject.age = newAge

        var consoleText = (i + 1) + '-' + newObject.firstname + ' ' + newObject.lastname + ' : '
        console.log(consoleText)
        console.log(newObject)
    }
}
console.log("== Soal No 1 ==")
// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)


// Tugas 2
console.log("=====Tugas 2=====")
function shoppingTime(memberId, money) {
    if(!memberId){
        return "Mohon maaf, Toko X hanay berlaku untuk member saja"
    } else if ( money < 50000){
        return "Mohon maaf anda tidak cukup"
    } else {
        var newobjek = {}
        var moneychange = money;
        var buyList = [];
        var sepatu = "Sepatu Stacatu"
        var bajuZoro = "Baju Zoro"
        var bajuHn = "Baju H&N"
        var sweater = "Sweater Uniqlooh"
        var casing = "Casing Handphone"

        var check = 0;
        for(var i = 0; moneychange >= 50000 && check == 0; i++){
            if (moneychange >= 1500000) {
                buyList.push(sepatu)
                moneychange -= 1500000
            } else if (moneychange >= 500000){
                buyList.push(bajuZoro)
                moneychange -= 500000
            } else if (moneychange >= 250000){
                buyList.push(bajuHn)
                moneychange -= 250000
            } else if (moneychange >= 175000){
                buyList.push(sweater)
                moneychange -= 175000
            } else if (moneychange >= 50000){
                for(var j=0; j <= buyList.length -1 ; j++){
                    if (buyList[j] == casing) {
                        check += 1
                    }
                } if (check == 0) {
                    buyList.push(casing)
                    moneychange -= 50000
                } else {
                    buyList.push(casing)
                    moneychange -= 50000
                }
            }
        }
        newobjek.memberId = memberId
        newobjek.money = money
        newobjek.buyList = buyList
        newobjek.moneychange = moneychange
        return newobjek
    }
}
console.log("=====Test2=====");
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// Tugas 3
console.log("=====Tugas 3=====");
//fungsi naikAngkot
function naikAngkot(arrPenumpang) {
    rute = ['A','B','C','D','E','F'];
    arrOutput = []
    if(arrPenumpang.length <= 0){
        return []
    }

    for(var i=0; i < arrPenumpang.length; i++){
        var ObjekOutput = {}
        var asal = arrPenumpang[i][1]
        var tujuan = arrPenumpang[i][2]
        //untuk menampung data index awal dan tujuan
        var indexasal;
        var indextujuan;
        for(var j = 0; j <rute.length; j++){
            if(rute[j] == asal){
                indexasal = j
            } else if(rute[j] == tujuan){
                indextujuan = j
            }
        }
        //untuk pembayaran
        var bayar = (indextujuan - indexasal) * 2000
        // variabel objek untuk mengembalikan nilai
        ObjekOutput.penumpang = arrPenumpang[i][0]
        ObjekOutput.naikDari = asal
        ObjekOutput.tujuan = tujuan
        ObjekOutput.bayar = bayar

        //mengambilak ke arrray
        arrOutput.push(ObjekOutput)
    }
    return arrOutput
}

console.log("=====Test 3=====");
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));


