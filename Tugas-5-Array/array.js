// Tugas Array 1
console.log("=====Tugas 1=====");

function range(startNum, finishNum) {
    var rangearray = [];

    if (startNum > finishNum) {
        var rangeLength = startNum - finishNum + 1;
        for (let i = 0; i < rangeLength; i++) {
            rangearray.push(startNum - i)
        }
    } else if(startNum < finishNum){
        var rangeLength = finishNum - startNum + 1;
        for (let i = 0; i < rangeLength; i++) {
            rangearray.push(startNum + i)
        }
    } else if( !startNum || !finishNum){
        return -1
    }
    return rangearray
}
console.log("=====test=====")
console.log(range(1, 10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range())

// Tugas Array 2 ==> range with step
console.log("=====Tugas 2=====");
    function rangeWithStep(startNum, finishNum, step) {
        var rangearray = [];
        if (startNum > finishNum) {
            var currentNum = startNum;
            for (let i = 0; currentNum >= finishNum; i++) {
                rangearray.push(currentNum)
                currentNum -= step
            }
        } else if(startNum < finishNum){
            var currentNum = startNum;
            for(var i = 0; currentNum <= finishNum; i++){
                rangearray.push(currentNum)
                currentNum += step
            }
        } else if(!startNum || !finishNum || step){
            return -1
        }
        return rangearray
    }
console.log("=====Test=====")
console.log(rangeWithStep(1,10,2))
console.log(rangeWithStep(11,23,3))
console.log(rangeWithStep(5,2,1))
console.log(rangeWithStep(29,2,4))

// Tugas Array 3
console.log("=====Tugas 3=====");
    function sum(startNum, finishNum, step) {
        var rangearray = [];
        var jarak;
        if (!step) {
            jarak=1
        } else {
            jarak = step    
        }
        //code perhitungan
        if (startNum > finishNum) {
            var currentNum = startNum;
            for(var i=0; currentNum >= finishNum; i++){
                rangearray.push(currentNum)
                currentNum -= jarak
            }
        } else if (startNum < finishNum){
            var currentNum = startNum;
            for(var i=0; currentNum <= finishNum; i++){
                rangearray.push(currentNum)
                currentNum += jarak
            }
        } else if( !startNum && !finishNum && !step){
            return 0
        } else if(startNum){
            return startNum
        }
        var total = 0;
        for(var i=0; i< rangearray.length; i++){
            total= total + rangearray[i]
        }
        return total
    }
console.log("=====Test=====")
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// Tugas Array 4
console.log("=====Tugas 4=====");
//array inputan
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
//coded splite
function dataHandling(data) {
    var dataLength = data.length
    for(var i=0; i<dataLength; i++){
        var id = "Nomor ID: "+ data[i][0]
        var Nama = "Nama Lengkap :"+ data[i][1]
        var Tanggallahir = "TTL : "+ data[i][2] + " "+data[i][3] 
        var Hobi = "Hobi : "+ data[i][4]

        console.log(id)
        console.log(Nama)
        console.log(Tanggallahir)
        console.log(Hobi)
    }
}
//input
console.log("=====Test=====")
dataHandling(input)

// Tugas Array 5
console.log("=====Tugas 5=====");
function balikKata(teks) {
    //menampung nilai awal
    var kata = " ";
    for(var i=teks.length-1; i >=0; i--){
        kata += teks[i]
    }
    return kata;
}
//hasil
console.log("=====Test=====");
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma

//Tugas 6
console.log("=====Tugas 6=====");
// input
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]  
dataHandling2(input);
// fungsi datahandling
function dataHandling2(data) {
    var newData = data
    var newName = data[1] + "Elsharawy"
    var newProvinsi = "Provinsi"+ data[2]
    var gender = "Pria"
    var institusi = "SMA Internasional Metro"

    newData.splice(1, 1, newName)
    newData.splice(2, 2, newProvinsi)
    newData.splice(4, 1, gender, institusi)

    var arrDate = data[3]
    var newDate = arrDate.split('/')
    var monthNum = newDate[1]
    var monthname = " "

    switch ( monthNum ) {
        case "01":
            monthname="Januari"
            break;
        case "02":
            monthname="Februari"
            break;
        case "03":
            monthname="Maret"
            break;
        case "04":
            monthname="April"
            break;
        case "05":
            monthname="Mei"
            break;
        case "06":
            monthname="Juni"
            break;
        case "07":
            monthname="Juli"
            break;
        case "08":
            monthname="Agustus"
            break;
        case "09":
            monthname="September"
            break;
        case "10":
            monthname="Oktober"
            break;
        case "11":
            monthname="November"
            break;
        case "12":
            monthname="Desember"
            break;
        default:
            break;
    }

    var datejoin = newDate.join("-")
    var dateArr = newDate.sort(function(value1, value2){
        value2 - value1
    })
    var editName = newName.slice(0, 15)
    console.log(newData)

    console.log(monthname)
    console.log(dateArr)
    console.log(datejoin)
    console.log(editName)
}
