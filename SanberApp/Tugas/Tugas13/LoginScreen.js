import React from 'react'
import { View, Text, Image, StyleSheet, TextInput, TouchableOpacity } from 'react-native'
import { AntDesign } from '@expo/vector-icons'; 

export default function LoginScreen(){
    return(
        <View style={styles.container}>
            <Image style={styles.logo} source={require('./image/logo.png')} />
            <Text style={styles.title}>Login</Text>
            <Text style={styles.tableName}>
                Username
            </Text>
               <View style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginHorizontal: 55,
                    borderWidth: 2,
                    marginTop: 15,
                    paddingHorizontal: 10,
                    borderColor: "#003366",
                    borderRadius: 23,
                    paddingVertical: 2
                }}>
                    <AntDesign name="mail" size={24} color="#003366" />
                    <TextInput style={{paddingHorizontal: 10}}/>
               </View>
            <Text style={styles.tableName}>
                Password
            </Text>
               <View style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginHorizontal: 55,
                    borderWidth: 2,
                    marginTop: 15,
                    paddingHorizontal: 10,
                    borderColor: "#003366",
                    borderRadius: 23,
                    paddingVertical: 2
                }}>
                    <AntDesign name="lock" size={24} color="#003366" />
                    <TextInput style={{paddingHorizontal: 10}} secureTextEntry={true}/>
               </View>

            <TouchableOpacity style={styles.button}>
                <Text style={{
                    color: "white", fontWeight: "bold"
                }}>Masuk</Text>
            </TouchableOpacity>

            <Text style={{textAlign: "center", fontSize: 14, marginTop: 8}}> atau </Text>

            <TouchableOpacity style={styles.buttonDaftar}>
                <Text style={{
                    color: "#003366", fontWeight: "bold"
                }}>Buat akun baru</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    logo: {
        alignItems: 'center',
        marginTop: '20%',
        resizeMode: 'stretch'
    },
    title: {
        textAlign: 'center',
        fontSize: 30,
        fontWeight: 'bold',
        paddingTop: 40,
        color: '#003366'
    },
    tableName: {
        marginTop: 25,
        marginLeft: 50,
        textAlign: 'left',
        fontSize: 18,
        fontWeight: 'bold'
    },
    button: {
        marginHorizontal: 55,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 15,
        backgroundColor: "#003366",
        paddingVertical: 10,
        borderRadius: 23
    },
    buttonDaftar: {
        marginHorizontal: 55,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 15,
        paddingVertical: 10,
        borderRadius: 23,
        borderColor: "#003366",
        borderWidth: 2
    }
})