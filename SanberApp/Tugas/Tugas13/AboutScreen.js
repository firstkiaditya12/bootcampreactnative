import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import { AntDesign } from '@expo/vector-icons'; 

export default function AboutScreen() {
    return(
        <View style={styles.container}>
            <Text style={styles.tittle}>Tentang Saya</Text>
            <Image style={styles.gambar} source={require('./image/profile4.png')}/>
            <Text style={styles.name}>Firstki Aditya</Text>
            <Text style={styles.name}>Backend Developer</Text>
                <View style={styles.box}>
                    <Text style={styles.textbox}>Portofolio</Text>
                    <View style={styles.boxicon}>
                        <AntDesign name="github" size={35} color="#003366" />
                        <AntDesign name="gitlab" size={35} color="#003366" />      
                    </View>
                    <View style={{
                        flexDirection: 'row',
                        marginTop: 10
                    }}>
                        <Text style={{
                            marginLeft: 22, marginRight: 22
                        }}>@firstkiadityaf12</Text>
                        <Text style={{
                            marginLeft: 22, marginRight: 22
                        }}>@firstkiadityaf12</Text>
                    </View>
                </View>

                <View style={styles.box}>
                    <Text style={styles.textbox}>Contact</Text>
                    <View style={styles.boxicon}>
                        <AntDesign name="linkedin-square" size={35} color="#003366" />
                        <AntDesign name="twitter" size={35} color="#003366" />      
                    </View>
                    <View style={{
                        flexDirection: 'row',
                        marginTop: 10
                    }}>
                        <Text style={{
                            marginLeft: 22, marginRight: 22
                        }}>Firstki Aditya Fenanda</Text>
                        <Text style={{
                            marginLeft: 22, marginRight: 22
                        }}>FFirstki12</Text>
                    </View>
                </View>
        </View>
    )
}

//styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center'
    },
    tittle: {
        fontSize: 25,
        fontWeight: 'bold',
        marginTop: 100,
        color: '#003366'
    },
    gambar: {
        width: 150,
        height: 150,
        borderRadius: 100,
        marginTop: 25
    },
    name: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 10,
        color: '#003366'
    },
    box: {
        height: 140,
        width: 300,
        backgroundColor:'#98c1d9',
        borderRadius: 20,
        marginTop: 10   
    },
    textbox: {
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 5
    },
    boxicon: {
        marginTop: 20,
        marginLeft: 50, marginRight: 50,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    textname: {
        textAlign: 'center'
    }
})

