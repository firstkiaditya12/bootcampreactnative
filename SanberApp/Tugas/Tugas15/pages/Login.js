import React from 'react'
import {StyleSheet, Text, View, Button} from 'react-native'

export default function Login(){
    return(
        <View style={styles.container}>
            <Text>Login</Text>
            <Button onPress={()=>NavigationPreloadManager.navigate("MyDrawwer",{
                screen: 'App', params: {
                    screen: 'Aboutscreen'
                }
            })} title="Menuju Halaman HomeScreen"/>
        </View>
    )
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})