import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';

import Aboutscreen from '../pages/AboutScreen';
import AddScreen from '../pages/AddScreen';
import HomeScreen from '../pages/Home';
import LoginScreen from '../pages/Login';
import ProjectScreen from '../pages/ProjectScreen';
import Setting from '../pages/Setting';
import SkillProject from '../pages/SkillProject';

const Tab = createBottomTabNavigator();
const Drawwer = createDrawerNavigator();
const Stack = createStackNavigator();


export default function Router() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="LoginScreen" component={LoginScreen}/>
                <Stack.Screen name="HomeScreen" component={HomeScreen}/>
                <Stack.Screen name="MainApp" component={MainApp}/>
                <Stack.Screen name="MyDrawwer" component={MyDrawwer}/> 
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const MainApp = () => {
    return(
        <Tab.Navigator>
            <Tab.Screen name="HomeScreen" component={HomeScreen} />
            <Tab.Screen name="HomeScreen" component={ProjectScreen} />
            <Tab.Screen name="SkillProject" component={SkillProject} />
        </Tab.Navigator>
    )
}

const MyDrawwer = () => {
    <Drawwer.Navigator>
        <Drawwer.Screen name="App" component={MainApp}/>
        <Drawwer.Screen name="AboutScreen" component={Aboutscreen}/>
    </Drawwer.Navigator>
}
