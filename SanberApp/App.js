import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Telegram from './Tugas/Tugas12/Telegram';
import LoginScreen from './Tugas/Tugas13/LoginScreen';
import AboutScreen from './Tugas/Tugas13/AboutScreen';
import RestApi from './Tugas/Tugas14/ReactApi';
import Index from './Tugas/Quiz3/index';
import Tugas15 from './Tugas/Tugas15/index';

export default function App() {
  return (
    //<Telegram/>
    //<LoginScreen/>
    //<AboutScreen/>
    //<RestApi/>
    //<Index/>
    <Tugas15/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#48cae4',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
