// Tugas 1 => Class
class Animal {
    // Code class di sini
    constructor(name){
        this._name = name;
        this._legs = 4;
        this._cold_blooded = false;
    }
    //memanggil atau mengembalikan
    get name(){
        return this._name
    }
    get legs(){
        return this._legs
    }
    set legs(hitung){
        return this._legs = hitung
    }
    get cold_blooded(){
        return this._cold_blooded
    }
}

var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// release 1
//mengakses name dan hitung
class App extends Animal{
    constructor(name, hitung){
        super(name)
        this._legs = hitung
    }
    yell(){
        console.log("AAUUOOO")
    }
}

class Frog extends Animal{
    constructor(name){
        super(name)
    }
    //fungsi
    jump(){
        console.log("Hop Hop")
    }
}

var sungokong = new App ("Kera Sakti", 2)
sungokong.yell()
console.log(sungokong.name)
console.log(sungokong.legs)
console.log(sungokong.cold_blooded)

//class kodong
var kodok = new Frog ("Buduk")
kodok.jump()
console.log(kodok.name)
console.log(kodok.legs)
console.log(kodok.cold_blooded)

// Tugas 2
class Clock {
    //constructurre
    constructor({ template }){
        this.template = template;
    }

    //fuction render => method
    render = () => {
        let date = new Date();
        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        let output = this.template.replace('h', hours).replace('m', mins).replace('s', secs);
        console.log(output)
    }
    stop(){
        clearInterval(this.timer);
    }
    start(){
        this.render();
        this.timer = setInterval(()=> this.render(), 1000);
    }
}
let Clock = new Clock({ template : 'h:m:s'});
Clock.start();

