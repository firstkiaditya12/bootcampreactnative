//Tugas 1
console.log("=====Tugas 1=====")
golden = () => {
    console.log("this is golden!!")
}
golden()

//TUGAS 2
console.log("=====Tugas 2=====")
const newFunction = (firstname, lastname) => {
    return {
        firstname, lastname,
        fullname(){
            console.log(firstname + " " + lastname)
            return
        }
    }
}
newFunction("William", "Imoh").fullname() 

// TUGAS 3
console.log("=====Tugas 3=====")

let newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occuption: "Dave-wizard Avocado",
    spell: "Vimulus Renderius!!"
}

const { firstName, lastName, destination, occuption, spell} = newObject
console.log(firstName, lastName, destination, occuption)

// TUGAS 4 COMBINED ARRAY
console.log("=====Tugas 4=====")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combinedArray = [...west, ...east]
console.log(combinedArray)

// TUGAS 5 TEMPLATE LITERALS
console.log("=====Tugas 5=====")
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit,${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam`
 
// Driver Code
console.log(before)

