// if else
var nama = "John";
var peran = ""

//logic
if (nama == '' && peran == '') {
    console.log("Nama harus diisi!");
} else  if(nama == 'John' && peran == ''){
    console.log("Halo John, Pilih peranmu untuk memulai game!");
} else if(nama == 'Jane' && peran == 'Penyihir'){
    console.log("Selamat datang di Dunia Werewolf, Jane");
    console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!");
} else if (nama == 'Jenita' && peran == 'Guard') {
    console.log("Selamat datang di Dunia Werewolf, Jenita");
    console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.");
} else if(nama == 'Junaedi' && peran == 'Warewolf'){
    console.log("Selamat datang di Dunia Werewolf, Junaedi");
    console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!");
}

// Switch
var tanggal; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

var hari = 21;
var bulan = 1;
var tahun = 1945;

switch(hari){
    case 1: hari = 1; break;
    case 2: hari = 2; break;
    case 3: hari = 3; break;
    case 4: hari = 4; break;
    case 5: hari = 5; break;
    case 6: hari = 6; break;
    case 7: hari = 7; break;
    case 8: hari = 8; break;
    case 9: hari = 8; break;
    case 10: hari = 10; break;
    case 11: hari = 11; break;
    case 12: hari = 12; break;
    case 13: hari = 13; break;
    case 14: hari = 14; break;
    case 15: hari = 15; break;
    case 16: hari = 16; break;
    case 17: hari = 17; break;
    case 18: hari = 18; break;
    case 19: hari = 19; break;
    case 20: hari = 20; break;
    case 21: hari = 21; break;
    case 22: hari = 22; break;
    case 23: hari = 23; break;
    case 24: hari = 24; break;
    case 25: hari = 25; break;
    case 26: hari = 26; break;
    case 27: hari = 27; break;
    case 28: hari = 28; break;
    case 29: hari = 29; break;
    case 30: hari = 30; break;
    case 31: hari = 31; break;
}

switch(bulan){
    case 1: bulan = "Januari"; break;
    case 2: bulan = "Februari"; break;
    case 3: bulan = "Maret"; break;
    case 4: bulan = "April"; break;
    case 5: bulan = "Mei"; break;
    case 6: bulan = "Juni"; break;
    case 7: bulan = "Juli"; break;
    case 8: bulan = "Agustus"; break;
    case 9: bulan = "September"; break;
    case 10: bulan = "Oktober"; break;
    case 11: bulan = "November"; break;
    case 12: bulan = "Desember"; break;
}

switch(tahun){
    case 1: tahun = 1945; break;
    case 2: tahun = 2003; break;
}

var hasil = hari + " " + bulan + " " + tahun;
console.log(hasil);